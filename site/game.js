guessnum = 1;
lastguess = 500000;
min = 1;
max = 1000000;
guessdelta = lastguess;

$(document).ready(function () {
	
	// play button
	$('#play').click(function() {
		min = parseInt($("#min").val());
		max = parseInt($("#max").val());
		var numguesses = Math.ceil(Math.log(max - min + 1) / Math.log(2));
		$("#numguesses").text(numguesses);
		$.mobile.changePage( "#gamepage" );
		guessnum = 1;
		lastguess = max - min + 1;
		guessdelta = lastguess;
		$("#next").attr('disabled', 'disabled');
		guess("high");
	});
	
	$('#answergroup input[type="radio"]').click(function() {
		$("#next").removeAttr('disabled');
	});
	
	$('#next').click(function() {
		// disable next guess button
		$("#next").attr('disabled', 'disabled');
		// get radio value
		guess($('#answergroup input[type="radio"]:checked').val());
		// check radios
		$('#answergroup input[type="radio"]').removeAttr('checked');
	});
	
});

function guess(answer) {
	var newguess = 0;
	
	guessdelta = guessdelta / 2;
	if (answer == "done") {
		win();
		return;
	} else if (answer == "high") {
		// too high, guess lower
		lastguess = lastguess - Math.round(guessdelta);
	} else if (answer == "low") {
		// too low, guess higher
		lastguess = lastguess + Math.round(guessdelta);
	}
	
	$("#guessnum").text(guessnum++);
	$("#guess").text(lastguess + min - 1);
	
}

function win() {
	$("#finalguess").text(lastguess);
	$.mobile.changePage( "#winpage" );
}
